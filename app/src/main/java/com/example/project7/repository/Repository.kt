package com.example.project7.repository

import com.example.project7.api.RetrofitInstance
import com.example.project7.model.geo.GeocodingItem
import com.example.project7.model.weatherOfCity.WeatherOfTheCity

class Repository {
    suspend fun getLocationOfCity(city:String): ArrayList<GeocodingItem> {
        return RetrofitInstance.api.getLocation(city)
    }

    suspend fun getWeatherOfTheCity(lat:String,lon:String): WeatherOfTheCity {
        return  RetrofitInstance.api.getWeather(lat,lon)
    }
}