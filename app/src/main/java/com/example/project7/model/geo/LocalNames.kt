package com.example.project7.model.geo

data class LocalNames(
    val ar: String,
    val en: String,
    val fr: String,
    val ko: String,
    val ru: String,
    val uk: String
)